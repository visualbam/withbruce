﻿using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Options;

namespace WithBruce.Services.Mail;

public class MailService(IOptions<MailSettings> mailSettings) : IMailService
{
    private readonly MailSettings _mailSettings = mailSettings.Value;
    public void SendMail(string? toAddress, string subject, string body)
    {
        var fromAddress = _mailSettings.Email;
        var mailPassword = _mailSettings.Password;

        using var mail = new MailMessage();
        mail.From = new MailAddress(fromAddress!);
        mail.To.Add(toAddress!);
        mail.Subject = subject;
        mail.Body = body;
        mail.IsBodyHtml = true;

        using var smtp = new SmtpClient("smtp.gmail.com", 587);
        smtp.Credentials = new NetworkCredential(fromAddress, mailPassword);
        smtp.EnableSsl = true;
        smtp.Send(mail);
    }
}