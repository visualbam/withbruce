﻿namespace WithBruce.Services.Mail;

public class MailSettings
{
    public string? Email { get; set; }
    public string? Password { get; set; }
}