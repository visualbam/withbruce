﻿namespace WithBruce.Services.Mail;

public interface IMailService
{
    void SendMail(string? toAddress, string subject, string body);
}