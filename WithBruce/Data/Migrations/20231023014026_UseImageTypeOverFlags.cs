﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WithBruce.Migrations
{
    /// <inheritdoc />
    public partial class UseImageTypeOverFlags : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCover",
                table: "ProjectImages");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "ProjectImages",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "ProjectImages");

            migrationBuilder.AddColumn<bool>(
                name: "IsCover",
                table: "ProjectImages",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
