﻿using MediatR;
using Microsoft.Extensions.Options;
using WithBruce.Services.Mail;
using WithBruce.Shared.ContactForm;

namespace WithBruce.Feature.ContactForm.Submit;

public class SubmitEmailCommandHandler(IMailService service, IOptions<MailSettings> mailSettings) : IRequestHandler<SubmitEmailCommand, ContactFormResponse>
{
    public Task<ContactFormResponse> Handle(SubmitEmailCommand request, CancellationToken cancellationToken)
    {
        try
        {
            service.SendMail(
                mailSettings.Value.Email,
                $"New contact from {request.Name}",
                $"<ul><li>Name: {request.Name}</li><li>Email: {request.EmailAddress}</li><li>Message: {request.Message}</li></ul>");
            return Task.FromResult(new ContactFormResponse() { Title = "Your message was successfully sent!", Message = "I will be getting in touch with you as soon as possible."});
        }
        catch (Exception)
        {
            return Task.FromResult(new ContactFormResponse() { Message = "There was an error processing your request.. please try again."});
        }
    }
}