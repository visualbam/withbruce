﻿using MediatR;
using WithBruce.Shared.ContactForm;

namespace WithBruce.Feature.ContactForm.Submit;

public class SubmitEmailCommand() : IRequest<ContactFormResponse>
{
    public string? Name { get; set; }
    public string? EmailAddress { get; set; }
    public string? Message { get; set; }
}