﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using WithBruce.Feature.ContactForm.Submit;
using WithBruce.Shared.ContactForm;

namespace WithBruce.Feature.ContactForm;

public static class ContactFormEndpoints
{
    public static void Map(WebApplication app)
    {
        app.MapPost("/api/contact-form", HandlePostContactFormAsync);
    }

    private static async Task<IResult> HandlePostContactFormAsync(SubmitEmailCommand contactForm, [FromServices] IMediator mediator)
    {
        var result = await mediator.Send(contactForm);
        return Results.Ok(result);
    }
}