﻿using MediatR;
using WithBruce.Shared.Experience;

namespace WithBruce.Feature.Experience.GetAll;

public class GetExperiencesQuery() : IRequest<IList<ExperienceResponse>>
{
}