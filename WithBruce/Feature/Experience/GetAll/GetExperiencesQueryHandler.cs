﻿using MediatR;
using WithBruce.Shared.Experience;

namespace WithBruce.Feature.Experience.GetAll;

public class GetExperiencesQueryHandler(IExperienceService service) : IRequestHandler<GetExperiencesQuery, IList<ExperienceResponse>>
{
    private readonly ExperienceMapper _mapper = new();

    public async Task<IList<ExperienceResponse>> Handle(GetExperiencesQuery request, CancellationToken cancellationToken)
    {
        var experiences = await service.GetExperiences();
        return experiences.Select(e => _mapper.MapExperienceToExperienceResponse(e)).ToList();
    }
}