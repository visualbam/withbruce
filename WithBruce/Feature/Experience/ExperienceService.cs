﻿using Microsoft.EntityFrameworkCore;
using WithBruce.Data;

namespace WithBruce.Feature.Experience;

public class ExperienceService(ApplicationDbContext dbContext) : IExperienceService
{
    private readonly ApplicationDbContext _dbContext = dbContext;

    public async Task<IList<Experience>> GetExperiences()
    {
        return await _dbContext.Experiences
            .OrderByDescending(x => x.EndDate == null)
            .ThenByDescending(x => x.EndDate)
            .ToListAsync();
    }
}