﻿namespace WithBruce.Feature.Experience;

public interface IExperienceService
{
    Task<IList<Experience>> GetExperiences();
}