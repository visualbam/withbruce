﻿using MediatR;
using WithBruce.Feature.Experience.GetAll;
using WithBruce.Shared.Experience;

namespace WithBruce.Feature.Experience;

public static class ExperienceEndpoints
{
    public static void Map(WebApplication app)
    {
        app.MapGet("/api/experiences", HandleGetExperiencesAsync);
    }

    private static async Task<IResult> HandleGetExperiencesAsync(IMediator mediator)
    {
        try
        {
            var response = await mediator.Send(new GetExperiencesQuery());
            return Results.Ok(response);
        }
        catch (Exception e)
        {
            return Results.Problem(detail: e.Message, statusCode: 500);
        }
    }
}