﻿using Riok.Mapperly.Abstractions;
using WithBruce.Shared.Experience;

namespace WithBruce.Feature.Experience;

[Mapper]
public partial class ExperienceMapper
{
    public ExperienceResponse MapExperienceToExperienceResponse(Experience experience)
    {
        return new ExperienceResponse
        {
            Id = experience.Id,
            Title = experience.Title,
            Company = experience.Company,
            StartDate = experience.StartDate,
            EndDate = experience.EndDate,
            Duration = experience.GetExperienceDuration(),
            Logo = experience.Logo
        };
    }
}