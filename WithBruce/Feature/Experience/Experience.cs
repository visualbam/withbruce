﻿namespace WithBruce.Feature.Experience;

public class Experience
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Company { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string? Logo { get; set; }

    public string GetExperienceDuration()
    {
        var endDate = EndDate ?? DateTime.Now;
        var diff = endDate - StartDate;
        var years = diff.Days / 365;
        var months = diff.Days % 365 / 30;

        return (years, months) switch
        {
            (0, _) => $"{months} mos",
            (_, 0) => $"{years} yr",
            _ => $"{years} yr {months} mos"
        };
    }
}