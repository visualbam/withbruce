﻿namespace WithBruce.Feature.Project;
using Tag;

public class Project
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public bool? IsFeatured { get; set; }
    public ICollection<Tag> Tags { get; set; } = new List<Tag>();
    public ICollection<ProjectImage> Images { get; set; } = new List<ProjectImage>();
}