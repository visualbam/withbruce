﻿using MediatR;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.GetPrevious;

public class GetPreviousProjectQuery(int currentProjectId) : IRequest<ProjectResponse>
{
    public readonly int CurrentProjectId = currentProjectId;
}