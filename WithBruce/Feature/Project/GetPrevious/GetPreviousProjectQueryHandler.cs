﻿using MediatR;
using WithBruce.Client.Services;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.GetPrevious;

public class GetPreviousProjectQueryHandler(IProjectService service, IAppSettings appSettings) : IRequestHandler<GetPreviousProjectQuery, ProjectResponse>
{
    private readonly ProjectMapper _mapper = new(appSettings);

    public async Task<ProjectResponse> Handle(GetPreviousProjectQuery request, CancellationToken cancellationToken)
    {
        var project = await service.GetPreviousProject(request.CurrentProjectId);
        return _mapper.MapProjectToProjectResponse(project);
    }
}