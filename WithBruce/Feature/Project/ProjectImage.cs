﻿using System.Text.Json.Serialization;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project;

public class ProjectImage
{
    public int Id { get; set; }
    public ImageType Type { get; set; }
    public string? FilePath { get; set; }

    public int ProjectId { get; set; }

    [JsonIgnore]
    public Project? Project { get; set; }
}