﻿using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project;

public interface IProjectService
{
    Task<List<Project>> GetProjects(ProjectFilterModel? filter = null);
    Task<Project> GetProject(string title);
    Task<Project> GetNextProject(int currentProjectId);
    Task<Project> GetPreviousProject(int currentProjectId);
}