﻿using MediatR;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.Get;

public class GetProjectQuery(string slug) : IRequest<ProjectResponse>
{
    public readonly string Slug = slug;
}