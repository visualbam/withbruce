﻿using MediatR;
using WithBruce.Client.Services;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.Get;

public class GetProjectQueryHandler(IProjectService service, IAppSettings appSettings) : IRequestHandler<GetProjectQuery, ProjectResponse>
{
    private readonly ProjectMapper _mapper = new(appSettings);

    public async Task<ProjectResponse> Handle(GetProjectQuery request, CancellationToken cancellationToken)
    {
        var project = await service.GetProject(request.Slug);
        return _mapper.MapProjectToProjectResponse(project);
    }
}