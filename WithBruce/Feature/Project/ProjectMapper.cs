﻿using Riok.Mapperly.Abstractions;
using WithBruce.Client.Services;
using WithBruce.Shared.Project;
using WithBruce.Shared.Tag;

namespace WithBruce.Feature.Project;

[Mapper]
public partial class ProjectMapper(IAppSettings appSettings)
{
    private IAppSettings AppSettings { get; } = appSettings;

    public ProjectResponse MapProjectToProjectResponse( Project project)
    {
        return new ProjectResponse()
        {
            Id = project.Id,
            Title = project.Title,
            Description = project.Description,
            IsFeatured = project.IsFeatured,
            Tags = project.Tags.Select(t => new TagDto()
            {
                Id = t.Id,
                Name = t.Name
            }).ToList(),
            Images = project.Images.Select(i => new ProjectImageDto()
            {
                Id = i.Id,
                Type = i.Type,
                FilePath = $"{AppSettings.MinioAddress}/{i.FilePath}",
                ProjectId = i.ProjectId
            }).Where(i => i.Type == ImageType.Image).ToList(),
            Logo = $"{AppSettings.MinioAddress}/{project.Images.ToList().FirstOrDefault(i => i.Type == ImageType.Logo)?.FilePath}",
            Cover = $"{AppSettings.MinioAddress}/{project.Images.ToList().FirstOrDefault(i => i.Type == ImageType.Cover)?.FilePath}",
            Mast = $"{AppSettings.MinioAddress}/{project.Images.ToList().FirstOrDefault(i => i.Type == ImageType.Mast)?.FilePath}"
        };
    }
}