﻿using Microsoft.EntityFrameworkCore;
using WithBruce.Data;
using WithBruce.Shared.Project;
using WithBruce.Shared.Slug;

namespace WithBruce.Feature.Project;

public class ProjectService(ApplicationDbContext dbContext, ISlugService slugService) : IProjectService
{
    public async Task<List<Project>> GetProjects(ProjectFilterModel? filter = null)
    {
        IQueryable<Project> query = dbContext.Projects;

        if (filter?.IncludeTags == true)
            query = query.Include(p => p.Tags);

        if (filter?.IsFeatured == true)
            query = query.Where(p => p.IsFeatured == filter.IsFeatured);

        query.Include(fp => fp.Images);

        query = query.Select(p => new Project
        {
            Id = p.Id,
            Title = p.Title,
            Description = p.Description,
            IsFeatured = p.IsFeatured,
            Images = p.Images,
            Tags = p.Tags.Select(t => new Tag.Tag
            {
                Id = t.Id,
                Name = t.Name
            }).ToList()
        });

        return await query.ToListAsync();
    }

    public async Task<Project> GetProject(string urlSlug)
    {
        var title = slugService.UnSlugify(urlSlug);
        return await dbContext.Projects
            .Include(p => p.Tags)
            .Include(p => p.Images)
            .FirstOrDefaultAsync(p => p.Title!.ToLower() == title.ToLower()) ?? throw new InvalidOperationException();
    }

    public async Task<Project> GetNextProject(int currentProjectId)
    {
        var nextProject = await dbContext.Projects
            .Include(p => p.Tags)
            .Include(p => p.Images)
            .Where(p => p.Id > currentProjectId)
            .OrderBy(p => p.Id)
            .FirstOrDefaultAsync();

        if (nextProject == null)
        {
            nextProject = await dbContext.Projects
                .Include(p => p.Tags)
                .Include(p => p.Images)
                .OrderBy(p => p.Id)
                .FirstOrDefaultAsync();
        }

        if (nextProject == null)
        {
            throw new InvalidOperationException("No next project found. This should not happen.");
        }

        return nextProject;
    }

    public async Task<Project> GetPreviousProject(int currentProjectId)
    {
        var previousProject = await dbContext.Projects
            .Include(p => p.Tags)
            .Include(p => p.Images)
            .Where(p => p.Id < currentProjectId)
            .OrderByDescending(p => p.Id)
            .FirstOrDefaultAsync();

        if (previousProject == null)
        {
            previousProject = await dbContext.Projects
                .Include(p => p.Tags)
                .Include(p => p.Images)
                .OrderByDescending(p => p.Id)
                .FirstOrDefaultAsync();
        }

        if (previousProject == null)
        {
            throw new InvalidOperationException("No previous project found. This should not happen.");
        }

        return previousProject;
    }
}