﻿using MediatR;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.GetAll;

public class GetProjectsQuery(ProjectFilterModel? filter) : IRequest<IList<ProjectResponse>>
{
    public readonly ProjectFilterModel? Filter = filter;
}