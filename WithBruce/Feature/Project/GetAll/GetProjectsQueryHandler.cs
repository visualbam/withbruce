﻿using MediatR;
using WithBruce.Client.Services;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.GetAll;

public class GetProjectsQueryHandler(IProjectService service, IAppSettings appSettings) : IRequestHandler<GetProjectsQuery, IList<ProjectResponse>>
{
    private readonly ProjectMapper _mapper = new(appSettings);

    public async Task<IList<ProjectResponse>> Handle(GetProjectsQuery request, CancellationToken cancellationToken)
    {
        var project = await service.GetProjects(request.Filter);
        return project.Select(p => _mapper.MapProjectToProjectResponse(p)).ToList();
    }
}