﻿using MediatR;
using WithBruce.Feature.Project.Get;
using WithBruce.Feature.Project.GetAll;
using WithBruce.Feature.Project.GetNext;
using WithBruce.Feature.Project.GetPrevious;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project;

public static class ProjectEndpoints
{
    public static void Map(WebApplication app)
    {
        var projects = app.MapGroup("/api/projects");
        projects.MapPost("/", HandleGetProjectsAsync);
        projects.MapGet("/{slug}", HandleGetProjectAsync);
        projects.MapGet("/{currentProjectId}/next", HandleGetNextProjectAsync);
        projects.MapGet("/{currentProjectId}/previous", HandleGetPreviousProjectAsync);
    }

    private static async Task<IResult> HandleGetProjectAsync(IMediator mediator, HttpContext context)
    {
        try
        {
            return context.Request.RouteValues["slug"] is string slug
                ? Results.Ok(await mediator.Send(new GetProjectQuery(slug)))
                : Results.BadRequest("Slug is required.");
        }
        catch (Exception e)
        {
            return Results.Problem(detail: e.Message, statusCode: 500);
        }
    }

    private static async Task<IResult> HandleGetNextProjectAsync(IMediator mediator, HttpContext context)
    {
        try
        {
            if (context.Request.RouteValues["currentProjectId"] is string currentProjectIdStr && int.TryParse(currentProjectIdStr, out var currentProjectId))
            {
                return Results.Ok(await mediator.Send(new GetNextProjectQuery(currentProjectId)));
            }

            return Results.BadRequest("Current Project ID is required.");
        }
        catch (Exception e)
        {
            return Results.Problem(detail: e.Message, statusCode: 500);
        }
    }

    private static async Task<IResult> HandleGetPreviousProjectAsync(IMediator mediator, HttpContext context)
    {
        try
        {
            if (context.Request.RouteValues["currentProjectId"] is string currentProjectIdStr && int.TryParse(currentProjectIdStr, out var currentProjectId))
            {
                return Results.Ok(await mediator.Send(new GetPreviousProjectQuery(currentProjectId)));
            }

            return Results.BadRequest("Current Project ID is required.");
        }
        catch (Exception e)
        {
            return Results.Problem(detail: e.Message, statusCode: 500);
        }
    }

    private static async Task<IResult> HandleGetProjectsAsync(IMediator mediator, HttpContext context)
    {
        try
        {
            var filter = await context.Request.ReadFromJsonAsync<ProjectFilterModel>();
            var projectDtos = await mediator.Send(new GetProjectsQuery(filter));
            return Results.Ok(projectDtos);
        }
        catch (Exception e)
        {
            return Results.Problem(detail: e.Message, statusCode: 500);
        }
    }
}