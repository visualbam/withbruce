﻿using MediatR;
using WithBruce.Client.Services;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.GetNext;

public class GetNextProjectQueryHandler(IProjectService service, IAppSettings appSettings) : IRequestHandler<GetNextProjectQuery, ProjectResponse>
{
    private readonly ProjectMapper _mapper = new(appSettings);

    public async Task<ProjectResponse> Handle(GetNextProjectQuery request, CancellationToken cancellationToken)
    {
        var project = await service.GetNextProject(request.CurrentProjectId);
        return _mapper.MapProjectToProjectResponse(project);
    }
}