﻿using MediatR;
using WithBruce.Shared.Project;

namespace WithBruce.Feature.Project.GetNext;

public class GetNextProjectQuery(int currentProjectId) : IRequest<ProjectResponse>
{
    public readonly int CurrentProjectId = currentProjectId;
}