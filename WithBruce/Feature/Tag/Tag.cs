﻿using System.Text.Json.Serialization;

namespace WithBruce.Feature.Tag;
using Project;

public class Tag
{
    public int Id { get; set; }
    public string? Name { get; set; }

    [JsonIgnore]
    public ICollection<Project> Projects { get; set; } = new List<Project>();
}