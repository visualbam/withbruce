window.theme = {
    toggleDarkMode: (value) => {
        if (value) {
            document.documentElement.classList.add('dark');
            localStorage.setItem('theme', 'dark')
        } else {
            document.documentElement.classList.remove('dark');
            localStorage.setItem('theme', 'light')
        }
    },
    getPrefersColorScheme: () => {
        return window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
    },
    observeHtmlMutation: () => {
        const targetNode = document.querySelector('html');
        window.addEventListener('beforeunload', () => observer.disconnect());
        const observer = new MutationObserver(() => {
            const currentTheme = localStorage.getItem('theme');
            if (currentTheme && !targetNode.classList.contains(currentTheme)) {
                targetNode.classList.add(currentTheme);
            }
        });
        observer.observe(targetNode, { attributes: true });
    }
};