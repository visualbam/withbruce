window.scrollFunctions = {
    scrollToTop: () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    },
    registerScrollEvent: function (dotnetHelper) {
        let last_known_scroll_position = 0;
        let ticking = false;

        window.onscroll = function(e) {
            last_known_scroll_position = window.scrollY;

            if (!ticking) {
                window.requestAnimationFrame(function() {
                    const winScroll = last_known_scroll_position;
                    const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                    const scrolled = (winScroll / height) * 100;

                    dotnetHelper.invokeMethodAsync('UpdateScrollProgress', scrolled);
                    ticking = false;
                });

                ticking = true;
            }
        };
    },
    registerEnhancedNavigationListener: () => {
        Blazor.addEventListener('enhancedload', () => {
            window.scrollTo({ top: 0, left: 0, behavior: 'instant' });
        });
    }
};