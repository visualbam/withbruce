/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
        './**/*.html',
        './**/*.cshtml',
        './**/*.razor',
    ],
    theme: {
        extend: {
            colors: {
                'slate-950': '#020617;'
            },
            fontFamily: {
                sans: [
                    'Roboto Slab',
                    'Roboto Condensed'
                ],
            }
        },
    },
    plugins: [],
}