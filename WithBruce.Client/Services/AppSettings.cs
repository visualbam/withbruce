﻿namespace WithBruce.Client.Services;

public class AppSettings(IConfiguration configuration) : IAppSettings
{
    public string? BaseAddress => configuration.GetValue<string>("WithBruceClient:BaseAddress");
    public string? MinioAddress => configuration.GetValue<string>("MinioClient:BaseAddress");
}