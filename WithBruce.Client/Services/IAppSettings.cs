﻿namespace WithBruce.Client.Services;

public interface IAppSettings
{
    string? BaseAddress { get; }
    string? MinioAddress { get; }
}