﻿using WithBruce.Client.Services.Theme;
using WithBruce.Shared.ContactForm;
using WithBruce.Shared.Experience;
using WithBruce.Shared.Project;
using WithBruce.Shared.Slug;

namespace WithBruce.Client.Services;

public static class CommonServices
{
    public static void ConfigureCommonServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IAppSettings, AppSettings>();
        services.AddScoped<IThemeService, ThemeService>();
        services.AddScoped<ISlugService, SlugService>();
        services.AddScoped<IExperienceClientService, ExperienceClientService>();
        services.AddScoped<IProjectClientService, ProjectClientService>();
        services.AddScoped<IContactFormClientService, ContactFormClientService>();

        services.AddHttpClient("WithBruceClient", c =>
        {
            c.BaseAddress = new Uri(configuration["WithBruceClient:BaseAddress"]!);
        });
    }
}