﻿using Blazored.LocalStorage;
using Microsoft.JSInterop;

namespace WithBruce.Client.Services.Theme
{
    public class ThemeService(IJSRuntime jsRuntime, ILocalStorageService localStorage) : IThemeService
    {
        public bool IsDarkMode { get; private set; }
        public event Action<bool>? OnThemeChanged;

        public async Task InitializeThemeAsync()
        {
            var themeValue = await localStorage.GetItemAsync<string>("theme");

            if (themeValue != null)
            {
                IsDarkMode = themeValue == "dark";
            }
            else
            {
                IsDarkMode = false;
            }

            OnThemeChanged?.Invoke(IsDarkMode);
            await ToggleThemeInBrowserAsync(IsDarkMode);
        }

        public async Task ToggleTheme()
        {
            IsDarkMode = !IsDarkMode;
            OnThemeChanged?.Invoke(IsDarkMode);
            await localStorage.SetItemAsync("theme", IsDarkMode ? "dark" : "light");
            await ToggleThemeInBrowserAsync(IsDarkMode);
        }

        private async Task ToggleThemeInBrowserAsync(bool isDarkMode)
        {
            await jsRuntime.InvokeVoidAsync("theme.toggleDarkMode", isDarkMode);
        }
    }
}