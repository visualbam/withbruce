﻿namespace WithBruce.Client.Services.Theme;

public interface IThemeService
{
    bool IsDarkMode { get; }
    event Action<bool> OnThemeChanged;
    Task InitializeThemeAsync();
    Task ToggleTheme();
}