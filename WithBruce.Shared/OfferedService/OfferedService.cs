﻿namespace WithBruce.Shared.OfferedService;

public class OfferedService
{
    public string? Name { get; set; }
    public string? ImagePath { get; set; }
    public string? AltText { get; set; }
}