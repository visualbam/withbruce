﻿using WithBruce.Shared.Tag;

namespace WithBruce.Shared.Project;

public class ProjectResponse
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public bool? IsFeatured { get; set; }
    public string? Cover { get; set; }
    public string? Logo { get; set; }
    public string? Mast { get; set; }
    public ICollection<TagDto> Tags { get; set; } = new List<TagDto>();
    public ICollection<ProjectImageDto> Images { get; set; } = new List<ProjectImageDto>();
}