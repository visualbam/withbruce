﻿namespace WithBruce.Shared.Project;

public class ProjectFilterModel
{
    public bool? IsFeatured { get; set; }
    public bool? IncludeTags { get; set; }
}