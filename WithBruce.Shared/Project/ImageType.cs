﻿namespace WithBruce.Shared.Project;

public enum ImageType
{
    Image,
    Cover,
    Logo,
    Mast
}