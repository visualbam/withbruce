﻿namespace WithBruce.Shared.Project;

public class ProjectImageDto
{
    public int Id { get; set; }
    public ImageType Type { get; set; }
    public string? FilePath { get; set; }
    public int ProjectId { get; set; }
}