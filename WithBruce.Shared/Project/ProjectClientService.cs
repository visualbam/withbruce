﻿using System.Net.Http.Json;
using System.Text.Json;
using WithBruce.Shared.Exceptions;

namespace WithBruce.Shared.Project;

public class ProjectClientService(IHttpClientFactory httpClientFactory) : IProjectClientService
{
    private readonly HttpClient _httpClient = httpClientFactory.CreateClient("WithBruceClient");

    public async Task<ProjectResponse> GetProjectAsync(string slug)
    {
        try
        {
            var response = await _httpClient.GetAsync($"/api/projects/{slug}");

            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException($"Request failed with status code {response.StatusCode}");

            var result = await response.Content.ReadFromJsonAsync<ProjectResponse>();

            if (result != null)
                return result;

            throw new ProjectNotFoundException($"Project with slug '{slug}' not found.");
        }
        catch (HttpRequestException ex)
        {
            throw new HttpException("Something went wrong while fetching the project.", ex);
        }

        catch (JsonException ex)
        {
            throw new InvalidDataException("Failed to deserialize the response to ProjectResponse.", ex);
        }
    }

    public async Task<ProjectResponse> GetNextProjectAsync(int currentProjectId)
    {
        try
        {
            var response = await _httpClient.GetAsync($"/api/projects/{currentProjectId}/next");

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Request failed with status code {response.StatusCode}");
            }

            var result = await response.Content.ReadFromJsonAsync<ProjectResponse>();

            if (result != null)
            {
                return result;
            }

            throw new ProjectNotFoundException($"No next project found for ID '{currentProjectId}'.");
        }
        catch (HttpRequestException ex)
        {
            throw new HttpException("Something went wrong while fetching the project.", ex);
        }
        catch (JsonException ex)
        {
            throw new InvalidDataException("Failed to deserialize the response to ProjectResponse.", ex);
        }
    }

    public async Task<ProjectResponse> GetPreviousProjectAsync(int currentProjectId)
    {
        try
        {
            var response = await _httpClient.GetAsync($"/api/projects/{currentProjectId}/previous");

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Request failed with status code {response.StatusCode}");
            }

            var result = await response.Content.ReadFromJsonAsync<ProjectResponse>();

            if (result != null)
            {
                return result;
            }

            throw new ProjectNotFoundException($"No previous project found for ID '{currentProjectId}'.");
        }
        catch (HttpRequestException ex)
        {
            throw new HttpException("Something went wrong while fetching the project.", ex);
        }
        catch (JsonException ex)
        {
            throw new InvalidDataException("Failed to deserialize the response to ProjectResponse.", ex);
        }
    }

    public async Task<IList<ProjectResponse>> GetProjectsAsync(ProjectFilterModel? filter)
    {
        try
        {
            filter ??= new ProjectFilterModel { IsFeatured = false, IncludeTags = false };
            var response = await _httpClient.PostAsJsonAsync("/api/projects", filter);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Request failed with status code {response.StatusCode}");
            }

            var projects = await response.Content.ReadFromJsonAsync<IList<ProjectResponse>>();

            if (projects == null || !projects.Any())
            {
                throw new ProjectNotFoundException("Projects were not found or unexpected response format");
            }

            return projects;
        }
        catch (HttpRequestException ex)
        {
            throw new HttpException("Something went wrong while fetching the projects.", ex);
        }
        catch (JsonException ex)
        {
            throw new InvalidDataException("Failed to deserialize the response to ProjectResponse list.", ex);
        }
    }
}