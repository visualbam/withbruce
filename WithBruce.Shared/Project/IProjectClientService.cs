﻿namespace WithBruce.Shared.Project;

public interface IProjectClientService
{
    Task<ProjectResponse> GetProjectAsync(string slug);
    Task<ProjectResponse> GetNextProjectAsync(int currentProjectId);
    Task<ProjectResponse> GetPreviousProjectAsync(int currentProjectId);
    Task<IList<ProjectResponse>> GetProjectsAsync(ProjectFilterModel? filter);
}