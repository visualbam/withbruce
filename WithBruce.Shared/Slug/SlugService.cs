﻿namespace WithBruce.Shared.Slug;

public class SlugService : ISlugService
{
    public string GenerateSlug(string name)
    {
        var slug = name.ToLowerInvariant().Replace(" ", "-");
        return slug;
    }

    public string UnSlugify(string slug)
    {
        return slug.Replace("-", " ");
    }
}