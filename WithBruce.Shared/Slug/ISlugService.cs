﻿namespace WithBruce.Shared.Slug;

public interface ISlugService
{
    string GenerateSlug(string name);
    string UnSlugify(string slug);
}