﻿using System.ComponentModel.DataAnnotations;

namespace WithBruce.Shared.ContactForm;

public class ContactFormRequest
{
    [Required]
    public string? Name { get; set; }
    [Required]
    public string? EmailAddress { get; set; }
    [Required]
    public string? Message { get; set; }
}