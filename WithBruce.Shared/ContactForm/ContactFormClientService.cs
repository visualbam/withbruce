﻿using System.Net.Http.Json;

namespace WithBruce.Shared.ContactForm;

public class ContactFormClientService(IHttpClientFactory client) : IContactFormClientService
{
    private readonly HttpClient _httpClient = client.CreateClient("WithBruceClient");

    public async Task<ContactFormResponse?> SubmitContactForm(ContactFormRequest request)
    {
        var response = await _httpClient.PostAsJsonAsync("/api/contact-form", request);
        if (response.IsSuccessStatusCode)
        {
            var contactFormResponse = await response.Content.ReadFromJsonAsync<ContactFormResponse>();
            return contactFormResponse;
        }
        else
        {
            throw new HttpRequestException($"Failed to submit contact form: { response.ReasonPhrase }");
        }
    }
}