﻿using System.ComponentModel.DataAnnotations;

namespace WithBruce.Shared.ContactForm;

public class ContactFormModel
{
    [Required]
    public required string Name { get; set; }

    [Required]
    public required string EmailAddress { get; set; }

    [Required]
    public required string Message { get; set; }
}