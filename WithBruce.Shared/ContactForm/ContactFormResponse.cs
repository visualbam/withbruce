﻿namespace WithBruce.Shared.ContactForm;

public class ContactFormResponse
{
    public string? Message { get; set; }
    public string? Title { get; set; }
}