namespace WithBruce.Shared.ContactForm;

public interface IContactFormClientService
{
    Task<ContactFormResponse?> SubmitContactForm(ContactFormRequest contactForm);
}