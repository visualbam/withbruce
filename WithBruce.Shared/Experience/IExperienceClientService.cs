﻿namespace WithBruce.Shared.Experience;

public interface IExperienceClientService
{

    public Task<IList<ExperienceResponse>?> GetExperiences();
}