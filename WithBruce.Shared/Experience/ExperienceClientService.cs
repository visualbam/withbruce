﻿using System.Net.Http.Json;

namespace WithBruce.Shared.Experience;

public class ExperienceClientService(IHttpClientFactory client) : IExperienceClientService
{
    private readonly HttpClient _httpClient = client.CreateClient("WithBruceClient");

    public async Task<IList<ExperienceResponse>?> GetExperiences()
    {
        var response = await _httpClient.GetFromJsonAsync<List<ExperienceResponse>>("/api/experiences");
        return response;
    }
}