﻿namespace WithBruce.Shared.Experience;

public class ExperienceResponse
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Company { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string? Logo { get; set; }
    public string? Duration { get; set; }
}